<?php

use Silex\Provider\MonologServiceProvider;

require __DIR__.'/config.php';

$app['debug'] = true;

$app->register(new MonologServiceProvider(), array(
	'monolog.logfile' => __DIR__.'/../var/logs/silex_dev.log',
));
