## Base de donnée MySQL

La base de donnée est fournie dans le dossier db, et préremplie avec quelques clients factices. Un utilisateur près à l'emploi permet de se servir de l'application, ses identifiants sont "test", ainsi que "test" pour le mot de passe.

## Technos

Les technos utilisées sont celles qui ont été demandées, à savoir composer dans un premier temps pour télecharger les sources, le framework Silex, Doctrine DBAL ainsi que Twig. Seul un ajout, très léger, de Foundation a été fait pour avoir un visuel plus sympathique.

## Défauts

Ayant bloqué sur les retours de codes http, j'ai laissé les retours de status de base. Je n'ai remarqué que trop tard qu'il était possible de mieux factoriser le code en utilisant différentes classes et la méthode mount(), et ai donc continué dans ma lancée initiale. Il manque également le tri par tranche d'âge, par soucis de manque de temps.