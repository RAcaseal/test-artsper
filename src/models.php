<?php

class ModelAPI{

	public $app;

	public function readUser($app, $data) {
		$query = $app['db']->createQueryBuilder()
		->select('u.*')
		->from('api_users', 'u')
		->where('u.api_username = ?')
		->andwhere('u.api_password = ?')
		->setParameter(0, $data['username'])
		->setParameter(1, $data['password'])
		;
		$user = $query->execute()->fetch();
		return $user;
	}

	public function countAll($app) {
		$query = $app['db']->createQueryBuilder()
		->select('COUNT(c.id) AS "count"')
		->from('api_customers', 'c')
		;
		$countCustomers = $query->execute()->fetch();
		return $countCustomers;
	}

	public function search($app, $keyword, $first, $max, $where = null, $sort = 'c.api_mail', $sortorder = 'ASC') {
		if (isset($where)) {
			$query = $app['db']->createQueryBuilder()
			->select('c.*')
			->from('api_customers', 'c')
			->where('c.api_name LIKE ?')
			->orwhere('c.api_firstname LIKE ?')
			->orwhere('c.api_mail LIKE ?')
			->andwhere('c.api_gender = ?')
			->orderBy($sort, $sortorder)
			->setParameter(0, '%'.$keyword.'%')
			->setParameter(1, '%'.$keyword.'%')
			->setParameter(2, '%'.$keyword.'%')
			->setParameter(3, $where)
			->setFirstResult($first)
			->setMaxResults($max)
			;
		} else {
			$query = $app['db']->createQueryBuilder()
			->select('c.*')
			->from('api_customers', 'c')
			->where('c.api_name LIKE ?')
			->orwhere('c.api_firstname LIKE ?')
			->orwhere('c.api_mail LIKE ?')
			->orderBy($sort, $sortorder)
			->setParameter(0, '%'.$keyword.'%')
			->setParameter(1, '%'.$keyword.'%')
			->setParameter(2, '%'.$keyword.'%')
			->setFirstResult($first)
			->setMaxResults($max)
			;
		}
		$results = $query->execute()->fetchAll();
		return $results;
	}

	public function readAll($app, $first, $max, $where = null, $sort = 'c.api_mail', $sortorder = 'ASC') {
		if (isset($where)) {
			$query = $app['db']->createQueryBuilder()
			->select('c.*')
			->from('api_customers', 'c')
			->where('c.api_gender = ?')
			->orderBy($sort, $sortorder)
			->setParameter(0, $where)
			->setFirstResult($first)
			->setMaxResults($max)
			;
		} else {
			$query = $app['db']->createQueryBuilder()
			->select('c.*')
			->from('api_customers', 'c')
			->orderBy($sort, $sortorder)
			->setFirstResult($first)
			->setMaxResults($max)
			;
		}
		$customers = $query->execute()->fetchAll();
		return $customers;
	}

	public function readSingle($app, $id) {
		$query = $app['db']->createQueryBuilder()
		->select('c.*')
		->from('api_customers', 'c')
		->where('c.id = ?')
		->setParameter(0, $id)
		;
		$customer = $query->execute()->fetch();
		return $customer;
	}

	public function addSingle($app, $data) {
		$query = $app['db']->createQueryBuilder()
		->insert('api_customers')
		->values(
			array(
				'api_name' => '?',
				'api_firstname' => '?',
				'api_birthdate' => '?',
				'api_gender' => '?',
				'api_mail' => '?',
				'api_phone' => '?',
				'api_street' => '?',
				'api_town' => '?',
				'api_zipcode' => '?',
				'api_country' => '?'
			)
		)
		->setParameter(0, $data['name'])
		->setParameter(1, $data['firstname'])
		->setParameter(2, $data['birthdate'])
		->setParameter(3, $data['gender'])
		->setParameter(4, $data['email'])
		->setParameter(5, $data['phone'])
		->setParameter(6, $data['street'])
		->setParameter(7, $data['city'])
		->setParameter(8, $data['zipcode'])
		->setParameter(9, $data['country'])
		;
		$query->execute();
		return 'Le client a bien été ajouté.';
	}

	public function editSingle($app, $data, $id) {
		$query = $app['db']->createQueryBuilder()
		->update('api_customers', 'c')
		->set('c.api_name', '?')
		->set('c.api_firstname', '?')
		->set('c.api_birthdate', '?')
		->set('c.api_gender', '?')
		->set('c.api_mail', '?')
		->set('c.api_phone', '?')
		->set('c.api_street', '?')
		->set('c.api_town', '?')
		->set('c.api_zipcode', '?')
		->set('c.api_country', '?')
		->setParameter(0, $data['name'])
		->setParameter(1, $data['firstname'])
		->setParameter(2, $data['birthdate'])
		->setParameter(3, $data['gender'])
		->setParameter(4, $data['email'])
		->setParameter(5, $data['phone'])
		->setParameter(6, $data['street'])
		->setParameter(7, $data['city'])
		->setParameter(8, $data['zipcode'])
		->setParameter(9, $data['country'])
		->where('c.id = '.$id)
		;
		$query->execute();
		return 'Le client a bien été modifié.';
	}

	public function deleteSingle($app, $id) {
		$query = $app['db']->delete('api_customers', array('id' => $id));
		return 'Le client a bien été supprimé.';
	}

	public function deleteMultiple($app, $array) {
		$query = $app['db']->executeQuery('DELETE FROM `api_customers` WHERE id IN (?)',
			array($array),
			array(\Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
		);
		return 'La sélection de clients a bin été supprimée.';
	}

}
