<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints as Assert;

function getAge($date) {
    $tdate = new DateTime($date);
    $now = new DateTime();
    $interval = $now->diff($tdate);
    return $interval->y;
}

function getGender($s1, $s2, $boolean, &$return) {
    if ($boolean == 0) {
        $return = $s1;
    } else {
        $return = $s2;
    }
}

// ------------------- LOGIN

$app->match('/', function(Request $request) use($app) {

    $default = array(
        'username' => '',
        'password' => ''
    );

    $form = $app['form.factory']->createBuilder(FormType::class, $default)
        ->add('username', TextType::class, array(
            'label' => 'Nom d\'utilisateur',
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Nom d\'utilisateur')
        ))
        ->add('password', PasswordType::class, array(
            'label' => 'Mot de passe',
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Mot de passe')
        ))
        ->getForm();

    $form->handleRequest($request);

    if($form->isValid()) {
        $data = $form->getData();
        $data['password'] = md5($data['password']);
        $model = new ModelAPI;
        $user = $model->readUser($app, $data);
        if (!empty($user['api_username'])
            && $data['username'] == $user['api_username']
            && !empty($user['api_password'])
            && $data['password'] == $user['api_password']) {
            $app['session']->set('user', array('username' => $user['api_username']));
            return $app->redirect($app['url_generator']->generate('listing'));
        }
        
    }
    
    return $app['twig']->render('index.html.twig', array(
        'form' => $form->createView(),
    ));
})
->bind('homepage');

$app->get('/logout', function() use($app) {
    $app['session']->clear();
    return $app->redirect($app['url_generator']->generate('homepage'));
})
->bind('logout');

// ------------------- LIST ALL

$app->get('/listing', function() use($app){

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    return $app->redirect($app['url_generator']->generate('listing').'/1');
})->bind('listing');

$app->match('/listing/{id}', function(Request $request, $id) use($app) {

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $model = new ModelAPI;
    $countCustomers = $model->countAll($app);

    $total = intval($countCustomers['count']);
    $perPage = 5;
    $nbrPages = ceil($total/$perPage);

    if (!empty($id)) {
        $page = intval($id);
        if ($page > $nbrPages) {
            $page = $nbrPages;
        }
    } else {
        $page = 1;
    }
    $firstEntry = ($page-1)*$perPage;

    $default = array(
        'gender' => '',
        'sort' => ''
    );

    $form = $app['form.factory']->createBuilder(FormType::class, $default)
    ->add('gender', ChoiceType::class, array(
        'choices' => array(
            'Sexe' => 1,
            'Homme' => 2,
            'Femme' => 3,
        ),
        'label' => 'Filtre par:',
        'attr' => array('class' => 'form-api large-6')
    ))
    ->add('sort', ChoiceType::class, array(
        'choices' => array(
            'Email croissant' => 1,
            'Email décroissant' => 2,
            'Nom croissant' => 3,
            'Nom décroissant' => 4,
            'Prénom croissant' => 5,
            'Prénom décroissant' => 6,
        ),
        'label' => 'Tri par:',
        'attr' => array('class' => 'form-api large-6')
    ))
    ->getForm();

    $form->handleRequest($request);

    if (!empty($request->query->all())) {
        $search = $request->query->all();
        $customers = $model->search($app, $search['search'], $firstEntry, $perPage);
        if (empty($customers)) {
            //$app->abort(404, "La recherche n'a pas donné de résultats sur cette page.");
            return $app->redirect($app['url_generator']->generate('listing'));
        }
    } elseif ($form->isValid() && $form->isSubmitted()) {
        $data = $form->getData();
        if (!empty($data['sort']) && !empty($data['gender'])) {

            $data['gender'] = ($data['gender'] == 1) ? null : $data['gender'];
            $data['gender'] = ($data['gender'] == 2) ? '0' : $data['gender'];
            $data['gender'] = ($data['gender'] == 3) ? '1' : $data['gender'];

            if ($data['sort'] == 1) {
                $data['sort'] = 'c.api_mail';
                $sortorder = 'ASC';
            } elseif ($data['sort'] == 2) {
                $data['sort'] = 'c.api_mail';
                $sortorder = 'DESC';
            } elseif ($data['sort'] == 3) {
                $data['sort'] = 'c.api_name';
                $sortorder = 'ASC';
            } elseif ($data['sort'] == 4) {
                $data['sort'] = 'c.api_name';
                $sortorder = 'DESC';
            } elseif ($data['sort'] == 5) {
                $data['sort'] = 'c.api_firstname';
                $sortorder = 'ASC';
            } elseif ($data['sort'] == 6) {
                $data['sort'] = 'c.api_firstname';
                $sortorder = 'DESC';
            }

            if (!empty($request->query->all())) {
                $customers = $model->search($app, $search['search'], $firstEntry, $perPage, $data['gender'], $data['sort'], $sortorder);
            } else {
                $customers = $model->readAll($app, $firstEntry, $perPage, $data['gender'], $data['sort'], $sortorder);
            }
        }
    } else {
        $customers = $model->readAll($app, $firstEntry, $perPage);
        if (empty($customers)) {
            //$app->abort(404, "Il n'y aucun clients.");
            return $app->redirect($app['url_generator']->generate('add'));
        }
    }

    foreach ($customers as $key => $value) {
        $customers[$key]['api_birthdate'] = getAge($value['api_birthdate']);
        getGender('Homme', 'Femme', $value['api_gender'], $customers[$key]['api_gender']);
    }

    if (!empty($request->request->get('global'))) {
        $results = $request->request->get('global');
        if (!empty($results['checkbox'])) {
            $model->deleteMultiple($app, $results['checkbox']);
        }
        return $app->redirect($app['url_generator']->generate('listing'));
    }

    return $app['twig']->render('global_listing.html.twig', array(
        'customers' => $customers,
        'page' => $id,
        'nbrpage' => $nbrPages,
        'form' => $form->createView(),
    ));
});

// ------------------- LIST SINGLE CUSTOMER

$app->get('/customer', function(Request $request) use($app){

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    return $app->redirect($app['url_generator']->generate('listing'));
})->bind('customer');

$app->get('/customer/{id}', function($id) use($app) {

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $model = new ModelAPI;
    $customer = $model->readSingle($app, $id);

    if (empty($customer)) {
        $app->abort(404, "Il n'y a pas de client référencé comme $id.");
    }

    getGender('masculin', 'féminin', $customer['api_gender'], $customer['api_gender']);
    $customer['api_birthdate'] = date('d/m/Y', strtotime($customer['api_birthdate']));

    return $app['twig']->render('single_listing.html.twig', array(
        'customer' => $customer,
    ));
});

// ------------------- ADD

$app->match('/add', function(Request $request) use($app) {

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $default = array(
        'name' => '',
        'firstname' => '',
        'birthdate' => '',
        'gender' => '',
        'email' => '',
        'phone' => '',
        'street' => '',
        "city" => '',
        'zipcode' => '',
        'country' => ''
    );

    $form = $app['form.factory']->createBuilder(FormType::class, $default)
        ->add('name', TextType::class, array(
            'label' => 'Nom de famille',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Nom de famille')
        ))
        ->add('firstname', TextType::class, array(
            'label' => 'Prénom',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Prénom')
        ))
        ->add('birthdate', DateType::class, array(
            'label' => 'Date de naissance',
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'd/M/y',
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'jj/mm/aaaa')
        ))
        ->add('gender', ChoiceType::class, array(
            'label' => 'Sexe',
            'choices' => array(
                'Homme' => 0,
                'Femme' => 1,
            ),
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Sexe')
        ))
        ->add('email', EmailType::class, array(
            'label' => 'Email',
            'constraints' => array(new Assert\Email(), new Assert\NotBlank(), new Assert\Length(array('min' => 6))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Email')
        ))
        ->add('phone', TextType::class, array(
            'label' => 'Téléphone',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Téléphone')
        ))
        ->add('street', TextType::class, array(
            'label' => 'Rue',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Rue')
        ))
        ->add('city', TextType::class, array(
            'label' => 'Ville',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Ville')
        ))
        ->add('zipcode', TextType::class, array(
            'label' => 'Code postal',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Code postal')
        ))
        ->add('country', TextType::class, array(
            'label' => 'Pays',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Pays')
        ))
        ->getForm();

    $form->handleRequest($request);

    if($form->isValid()) {
        $data = $form->getData();
        $model = new ModelAPI;
        $model->addSingle($app, $data);
        return $app->redirect($app['url_generator']->generate('listing'));
    }

    return $app['twig']->render('add_user.html.twig', array(
        'form' => $form->createView(),
    ));
})
->bind('add');

// ------------------- EDIT

$app->get('/edit', function() use($app){

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    return $app->redirect($app['url_generator']->generate('listing'));
})->bind('edit');

$app->match('/edit/{id}', function(Request $request, $id) use($app) {

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $app['current_url'] = $_SERVER['REQUEST_URI'];
    $model = new ModelAPI;
    $customer = $model->readSingle($app, $id);

    if (empty($customer)) {
        $app->abort(404, "Il n'y a pas de client référencé comme $id.");
    }

    $default = array(
        'name' => $customer['api_name'],
        'firstname' => $customer['api_firstname'],
        'birthdate' => $customer['api_birthdate'],
        'gender' => $customer['api_gender'],
        'email' => $customer['api_mail'],
        'phone' => $customer['api_phone'],
        'street' => $customer['api_street'],
        "city" => $customer['api_town'],
        'zipcode' => $customer['api_zipcode'],
        'country' => $customer['api_country']
    );

    $form = $app['form.factory']->createBuilder(FormType::class, $default)
        ->add('name', TextType::class, array(
            'label' => 'Nom de famille',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Nom de famille')
        ))
        ->add('firstname', TextType::class, array(
            'label' => 'Prénom',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Prénom')
        ))
        ->add('birthdate', DateType::class, array(
            'label' => 'Date de naissance',
            'widget' => 'single_text',
            'input' => 'string',
            'format' => 'd/M/y',
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'xx/xx/xxxx')
        ))
        ->add('gender', ChoiceType::class, array(
            'label' => 'Sexe',
            'choices' => array(
                'Homme' => 0,
                'Femme' => 1,
            ),
            'constraints' => array(new Assert\NotBlank()),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Sexe')
        ))
        ->add('email', EmailType::class, array(
            'label' => 'Email',
            'constraints' => array(new Assert\Email(), new Assert\NotBlank(), new Assert\Length(array('min' => 6))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Email')
        ))
        ->add('phone', TextType::class, array(
            'label' => 'Téléphone',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Téléphone')
        ))
        ->add('street', TextType::class, array(
            'label' => 'Rue',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Rue')
        ))
        ->add('city', TextType::class, array(
            'label' => 'Ville',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Ville')
        ))
        ->add('zipcode', TextType::class, array(
            'label' => 'Code postal',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Code postal')
        ))
        ->add('country', TextType::class, array(
            'label' => 'Pays',
            'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 3))),
            'attr' => array('class' => 'form-api', 'placeholder' => 'Pays')
        ))
        ->getForm();

    $form->handleRequest($request);

    if($form->isValid()) {
        $data = $form->getData();
        $model->editSingle($app, $data, $id);
        return $app->redirect($app['url_generator']->generate('listing'));
    }
    
    return $app['twig']->render('edit_user.html.twig', array(
        'customer' => $customer,
        'form' => $form->createView()
    ));
});

// ------------------- DELETE

$app->get('/delete', function() use($app){

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    return $app->redirect($app['url_generator']->generate('listing'));
})->bind('delete');

$app->get('/delete/{id}', function($id) use($app) {

    if (empty($app['session']->get('user'))) {
        return $app->redirect($app['url_generator']->generate('homepage'));
    }

    $model = new ModelAPI;
    $customer = $model->readSingle($app, $id);

    if (empty($customer)) {
        return $app->redirect($app['url_generator']->generate('listing'));
    }

    $model->deleteSingle($app, $id);
    return $app->redirect($app['url_generator']->generate('listing'));
});

// ------------------- ERRORS

$app->error(function (\Exception $e, Request $request, $code) use ($app) {

    if ($app['debug']) {
        return;
    }

    return new Response($app['twig']->render('errors.html.twig', array(
        'code' => $code
    )), $code);
});
