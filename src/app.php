<?php

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\RoutingServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\FormServiceProvider;
use Silex\Provider\TranslationServiceProvider;

$app = new Application();

$app->register(new SessionServiceProvider());
$app->register(new DoctrineServiceProvider(), array(
	'db.options' => array(
		'driver'	=> 'pdo_mysql',
		'host'		=> 'localhost',
		'dbname'	=> 'apiartsper',
		'user'		=> 'root',
		'password'	=> '',
		),
));
$app->register(new RoutingServiceProvider());
$app->register(new TwigServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new FormServiceProvider());
$app->register(new TranslationServiceProvider(), array(
	'translator.domains' => array(),
	'locale' => 'fr'
));

$app['twig'] = $app->extend('twig', function($twig, $app) {
	return $twig;
});

return $app;
