-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 29 Août 2016 à 13:31
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `apiartsper`
--
CREATE DATABASE IF NOT EXISTS `apiartsper` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `apiartsper`;

-- --------------------------------------------------------

--
-- Structure de la table `api_customers`
--

CREATE TABLE `api_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `api_name` varchar(255) NOT NULL,
  `api_firstname` varchar(255) NOT NULL,
  `api_birthdate` date NOT NULL,
  `api_gender` tinyint(1) NOT NULL,
  `api_mail` varchar(255) NOT NULL,
  `api_phone` varchar(30) NOT NULL,
  `api_street` varchar(255) NOT NULL,
  `api_town` varchar(255) NOT NULL,
  `api_zipcode` varchar(30) NOT NULL,
  `api_country` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `api_customers`
--

INSERT INTO `api_customers` (`id`, `api_name`, `api_firstname`, `api_birthdate`, `api_gender`, `api_mail`, `api_phone`, `api_street`, `api_town`, `api_zipcode`, `api_country`) VALUES
(1, 'Bertrand', 'Jean', '2001-06-08', 0, 'bertrandjean@email.fr', '0123456789', '123 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(2, 'Thomas', 'Katie', '1991-06-08', 1, 'thomaskatie@email.fr', '0133456789', '113 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(3, 'Fouchou', 'Ethan', '1954-06-08', 0, 'fouchouethan@email.fr', '0123256789', '103 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(4, 'Aucanard', 'Lamar', '2009-06-08', 0, 'lamaraucanard@email.fr', '0123457489', '93 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(5, 'Jokerini', 'Batou', '1930-06-08', 0, 'jokerinibatou@email.fr', '0111111111', '03 Rue HAHAHAHA', 'Gotham City', '11111', 'Terre'),
(6, 'Cheveux', 'AmÃ©lie', '1995-06-08', 1, 'ameliecheveux@email.fr', '0144457489', '83 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(7, 'Traore', 'AimÃ©', '1984-06-08', 0, 'traoreaime@email.fr', '0123457488', '23 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(8, 'Melade', 'Lamar', '1941-06-08', 0, 'lamarmelade@email.fr', '0123557489', '73 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(9, 'Motte', 'Lamar', '2002-06-08', 0, 'lamarmotte@email.fr', '0613456789', '63 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(10, 'Mitte', 'Lamar', '2009-06-08', 0, 'lamarmitte@email.fr', '0623457489', '53 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(11, 'Seillaise', 'Lamar', '2009-06-08', 0, 'lamarseillaise@email.fr', '0198765432', '53 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(12, 'Chandise', 'Lamar', '2003-06-08', 0, 'lamarchandise@email.fr', '0123457489', '43 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(13, 'Sophetia', 'Camille', '1976-06-08', 0, 'sophetiacamille@email.fr', '0123457489', '93 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(14, 'Ijuana', 'Lamar', '2009-06-08', 0, 'lamarijuana@email.fr', '0123457489', '33 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(15, 'Oquinerie', 'Lamar', '2009-06-08', 0, 'lamaroquinerie@email.fr', '0123457489', '13 Rue Unendroit', 'Villecity', '11111', 'Terre'),
(16, 'Guerite', 'Lamar', '2009-06-08', 0, 'lamarguerite@email.fr', '0123457489', '93 Rue Unendroit', 'Villecity', '11111', 'Terre');

-- --------------------------------------------------------

--
-- Structure de la table `api_users`
--

CREATE TABLE `api_users` (
  `id` int(11) NOT NULL,
  `api_username` varchar(255) NOT NULL,
  `api_password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `api_users`
--

INSERT INTO `api_users` (`id`, `api_username`, `api_password`) VALUES
(1, 'test', '098f6bcd4621d373cade4e832627b4f6');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `api_customers`
--
ALTER TABLE `api_customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `api_mail` (`api_mail`);

--
-- Index pour la table `api_users`
--
ALTER TABLE `api_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`api_username`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `api_customers`
--
ALTER TABLE `api_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT pour la table `api_users`
--
ALTER TABLE `api_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
